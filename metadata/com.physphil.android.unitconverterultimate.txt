Categories:Science & Education
License:Apache2
Web Site:
Source Code:https://github.com/physphil/UnitConverterUltimate
Issue Tracker:https://github.com/physphil/UnitConverterUltimate/issues

Auto Name:Unit Converter Ultimate
Summary:Convert between units with ease
Description:
A simple and easy-to-use unit converter to handle any conversion you'll ever
need.

The beautiful Material Design user interface allows for quick and easy
conversions from a number in one unit to another. The goal is to keep it simple
- you won't be overwhelmed with an excess of options and settings, allowing you
to perform your desired conversion as quickly as possible. Perfect for work,
school or in the kitchen.
.

Repo Type:git
Repo:https://github.com/physphil/UnitConverterUltimate

Build:4.2,40200
    commit=v4.2
    subdir=app
    gradle=yes
    rm=app/src/main/java/com/physphil/android/unitconverterultimate/DonateActivity.java,app/src/main/java/com/physphil/android/unitconverterultimate/ui/DonationListAdapter.java
    prebuild=perl -0 -p -i -e 's|<Preference\s[^<]*<intent\s[^<]*\.DonateActivity".*?</Preference>\s*||s' app/src/main/res/xml/preferences.xml && \
        perl -0 -p -i -e 's|<activity\s[^<]*"\.DonateActivity".*?/>\s*||s' app/src/main/AndroidManifest.xml

Build:5.0,50000
    disable=firebase
    commit=v5.0
    subdir=app
    gradle=yes
    rm=app/src/main/java/com/physphil/android/unitconverterultimate/DonateActivity.java,app/src/main/java/com/physphil/android/unitconverterultimate/ui/DonationListAdapter.java
    prebuild=perl -0 -p -i -e 's|<Preference\s[^<]*<intent\s[^<]*\.DonateActivity".*?</Preference>\s*||s' app/src/main/res/xml/preferences.xml && \
        perl -0 -p -i -e 's|<activity\s[^<]*"\.DonateActivity".*?/>\s*||s' app/src/main/AndroidManifest.xml

Build:5.0.1,50001
    disable=firebase
    commit=v5.0
    subdir=app
    gradle=yes
    rm=app/src/main/java/com/physphil/android/unitconverterultimate/DonateActivity.java,app/src/main/java/com/physphil/android/unitconverterultimate/ui/DonationListAdapter.java
    prebuild=perl -0 -p -i -e 's|<Preference\s[^<]*<intent\s[^<]*\.DonateActivity".*?</Preference>\s*||s' app/src/main/res/xml/preferences.xml && \
        perl -0 -p -i -e 's|<activity\s[^<]*"\.DonateActivity".*?/>\s*||s' app/src/main/AndroidManifest.xml

Maintainer Notes:
Upstream intents to fix the firebase stuff after 5.0.1, see
https://github.com/physphil/UnitConverterUltimate/issues/13
-- so dont set UpstreamNonFree for now.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:5.0.1
Current Version Code:50001
